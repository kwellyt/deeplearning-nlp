import tensorflow as tf


def NCE_loss(W_p, bias, output_words, h, neg_sample, word_size):
    loss = tf.nn.nce_loss(W_p, bias, output_words, h, neg_sample, word_size)
    loss = tf.reduce_mean(loss)
    return loss

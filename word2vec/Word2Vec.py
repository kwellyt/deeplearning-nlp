import tensorflow as tf
from Losses import NCE_loss
from utils import dataset, load_enc_dec, path_check


class word2vec:
    def __init__(self, params, optimizer, SG=True, trainable=True):
        tf.reset_default_graph()
        self.params = params
        self.word_size = params['word_size']
        self.embedding_size = params['embedding_size']
        self.window_size = params['window_size']
        self.batch_size = params['batch_size']
        self.neg_sample = params['neg_sample']
        self.learning_rate = params['learning_rate']
        self.epochs = params['epochs']
        self.optimizer = optimizer(self.learning_rate)

        with tf.variable_scope('word2vec', reuse=tf.AUTO_REUSE):
            if SG:
                with tf.variable_scope('Placeholder', reuse=tf.AUTO_REUSE):
                    self.input_words = tf.placeholder(tf.int32, [None, ], 'input_words')
                    self.output_words = tf.placeholder(tf.int32, [None, ], 'output_words')
                    self.output_words = tf.reshape(self.output_words, [-1, 1])

                with tf.variable_scope('Parameter', reuse=tf.AUTO_REUSE):
                    self.W = tf.get_variable('input_vector', [self.word_size, self.embedding_size],
                                             initializer=tf.truncated_normal_initializer())
                    self.W_p = tf.get_variable('output_vector', [self.word_size, self.embedding_size],
                                               initializer=tf.truncated_normal_initializer())
                    self.bias = tf.get_variable('bias', [self.word_size],
                                                initializer=tf.zeros_initializer())

                with tf.variable_scope('Embedding', reuse=tf.AUTO_REUSE):
                    self.h = tf.nn.embedding_lookup(self.W, self.input_words)  # [batch_size, embedding_size]
            else:
                with tf.variable_scope('Placeholder', reuse=tf.AUTO_REUSE):
                    self.input_words = tf.placeholder(tf.int32, [None, self.window_size * 2], 'input_words')
                    self.output_words = tf.placeholder(tf.int32, [None, ], 'output_words')
                    self.output_words = tf.reshape(self.output_words, [-1, 1])

                with tf.variable_scope('Parameter', reuse=tf.AUTO_REUSE):
                    self.W = tf.get_variable('input_vector', [self.word_size, self.embedding_size],
                                             initializer=tf.truncated_normal_initializer())
                    self.W_p = tf.get_variable('output_vector', [self.embedding_size, self.word_size],
                                               initializer=tf.truncated_normal_initializer())
                    self.bias = tf.get_variable('bias', [self.word_size], initializer=tf.zeros_initializer())

                with tf.variable_scope('Embedding', reuse=tf.AUTO_REUSE):
                    self.h = tf.nn.embedding_lookup(self.W, self.input_words)  # [batch_size, window_size * 2, embedding_size]
                    self.h = tf.reduce_mean(self.h, 1)  # [batch_size, embedding_size])

            if trainable:
                self.dataset_init, self.dataset_next = dataset(params['data_path'],
                                                               self.batch_size,
                                                               self.window_size,
                                                               SG=SG)
                with tf.variable_scope('optimize', reuse=tf.AUTO_REUSE):
                    self.loss = NCE_loss(self.W_p, self.bias, self.output_words,
                                         self.h, self.neg_sample, self.word_size)
                    self.update = self.optimizer.minimize(self.loss)
            else:
                self.input_words = tf.placeholder(tf.int32, [None, ], 'input_words')
                self.encoder, self.decoder = load_enc_dec()
        return

    def train(self):
        path_check(self.params['model_path'])
        with tf.Session() as sess:
            self.saver = tf.train.Saver(max_to_keep=5)
            init = [tf.global_variables_initializer(), self.dataset_init]
            sess.run(init)
            total_loss = 0
            batch_count = 0

            for epoch in range(self.epochs):
                while True:
                    try:
                        batch_input, batch_output = sess.run(self.dataset_next)
                    except tf.errors.OutOfRangeError:
                        break

                    __, loss = sess.run([self.update, self.loss],
                                        feed_dict={self.input_words: batch_input,
                                                   self.output_words: batch_output})
                    total_loss += loss
                    batch_count += 1
                print('Epoch: %04d Loss: %.4f' % (epoch + 1, total_loss / batch_count))

                if (epoch + 1) % 10 == 0:
                    self.saver.save(sess, self.params['model_path'] + 'model_%d' % (epoch + 1))
        return

    def embedding(self, input_words):
        with tf.Session() as sess:
            try:
                ckpt = tf.train.get_checkpoint_state(self.params['model_path'])
                self.saver.restore(sess, ckpt.model_checkpoint_path)
            except Exception:
                print('There are no model checkpoint.')

            if type(input_words) == 'list':
                input_words = [self.encoder(w) for w in input_words]
            else:
                input_words = self.encoder(input_words)
            embedding = tf.nn.embedding_lookup(self.W, self.input_words)
            vector = sess.run(embedding, feed_dict={self.input_words: input_words})
        return vector

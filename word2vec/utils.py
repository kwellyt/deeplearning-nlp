import tensorflow as tf
import pandas as pd
import os


def path_check(path):
    if os.path.exists(path) is False:
        os.mkdir(path)
    return


def generate_SG(document, window_size):
    result = []
    for sentence in document:
        if type(sentence) == 'str':
            sentence = sentence.split(' ')
        sentence_len = len(sentence)
        for i in range(window_size, sentence_len - window_size):
            window = sentence[(i - window_size):(i + window_size + 1)]
            center_word = window.pop(window_size)
            context_word = window
            center_word = [center_word for i in range(len(context_word))]

            for center, context in zip(center_word, context_word):
                result.append((center, context))
    return result


def generate_CBOW(document, window_size):
    result = []
    for sentence in document:
        if type(sentence) == 'str':
            sentence = sentence.split(' ')
        sentence_len = len(sentence)
        for i in range(window_size, sentence_len - window_size):
            window = sentence[(i - window_size):(i + window_size + 1)]
            center_word = window.pop(window_size)
            context_word = window
            result.append((context_word, center_word))
    return result


def dataset(data_path, window_size, batch_size, SG=True):
    document = pd.read_pickle(data_path)
    path_check('./data/')
    if SG:
        document = generate_SG(document, window_size)
    else:
        document = generate_CBOW(document, window_size)

    input_words = tf.data.Dataset.from_tensor_slices([d[0] for d in document])
    output_words = tf.data.Dataset.from_tensor_slices([d[1] for d in document])
    dataset = tf.data.Dataset.zip((input_words, output_words))
    dataset = dataset.shuffle(1000000).batch(batch_size)
    iterator = dataset.make_initializable_iterator()
    next_element = iterator.get_next()
    dataset_init_op = iterator.initializer
    return dataset_init_op, next_element


def mk_enc_dec(document):
    bag = set()
    for doc in document:
        bag = bag | set(doc)
    bag = sorted(list(bag))
    encoder = {w: i for i, w in enumerate(bag)}
    decoder = {i: w for i, w in enumerate(bag)}
    print(len(encoder), 'words')
    pd.to_pickle(encoder, './data/encoder.pkl')
    pd.to_pickle(decoder, './data/decoder.pkl')
    return encoder, decoder


def load_enc_dec():
    encoder = pd.read_pickle('./data/encoder.pkl')
    decoder = pd.read_pickle('./data/decoder.pkl')
    return encoder, decoder

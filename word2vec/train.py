from Word2Vec import word2vec
import tensorflow as tf


if __name__ == '__main__':
    params = {'word_size': 121548,
              'embedding_size': 100,
              'window_size': 1,
              'batch_size': 128,
              'neg_sample': 1000,
              'learning_rate': 0.001,
              'epochs': 1000,
              'model_path': './model/',
              'data_path': './data/wikipedia.pkl'}
    optimizer = tf.train.GradientDescentOptimizer
    model = word2vec(params, optimizer, SG=True, trainable=True)
    model.train()
